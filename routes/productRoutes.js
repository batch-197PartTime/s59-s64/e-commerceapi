const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

// Route to create product (admin only)
router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)	
	if(userData.isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
});

// Route to retrieve all products
router.get("/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all active products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// Route to retrieve singe product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId)
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Route to update product information (admin only)
router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
});


// Route to archive
router.put('/archive/:productId', auth.verify, (req, res) => {
	const data = {
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});

router.put('/active/:productId', auth.verify, (req, res) => {
	const data = {
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.activeProduct(data).then(resultFromController => res.send(resultFromController))
});



module.exports = router;